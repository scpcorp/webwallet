#!/usr/bin/env bash

echo "check-builds is used to check that the Web Wallet can be compiled on all supported systems"

# Create fresh artifacts folders.
rm -rf artifacts/linux
mkdir -p artifacts/linux/amd64

rm -rf artifacts/windows
mkdir -p artifacts/windows/amd64

rm -rf tmp
mkdir tmp

# Return first error encountered by any command.
set -e

# Build the wallet.wasm resource
echo Building wallet.wasm
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ./resources/resources/wasm_exec.js
GOOS=js GOARCH=wasm go build -o ./resources/resources/wallet.wasm ./cmd/wasm/main.go

echo building for linux
for pkg in scp-webwallet scp-webwallet-server; do
    echo ${pkg}...
    fyne package --release --os linux --tags 'netgo' --src ./cmd/${pkg} --icon $(pwd)/resources/resources/icon.png
    tar xf ${pkg}.tar.xz -C tmp usr/local/bin/${pkg}
    mv tmp/usr/local/bin/${pkg} artifacts/linux/amd64
done

WW_APP_ID=me.scpri.webwallet
echo building for windows
for pkg in scp-webwallet scp-webwallet-server; do
    echo ${pkg}...
    CC=x86_64-w64-mingw32-gcc fyne package --release --os windows --appID ${WW_APP_ID} --src ./cmd/${pkg} --icon $(pwd)/resources/resources/icon.png
    mv cmd/${pkg}/${pkg}.exe artifacts/windows/amd64
done