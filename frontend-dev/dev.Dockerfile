FROM alpine:latest

LABEL application="Vue Development Environment"
LABEL version="1.0.0"
LABEL type="Dev"
LABEL maintainer="Addramyr.uk"

RUN mkdir -p /app/env

RUN apk add yarn python3 make gcc
RUN apk add --update --no-cache bash wget nodejs npm jq openssl curl logrotate zip unzip eudev-dev linux-headers pcsc-lite pcsc-lite-libs pcsc-lite-dev build-base make g++ usbutils
RUN apk add --update bash wget nodejs npm jq openssl curl git
RUN apk add mc nano htop ncdu

WORKDIR /app

RUN npm install --global yarn

COPY ./data/startup.sh /app/startup.sh
RUN ls -lah /app
RUN chmod +x /app/startup.sh

CMD /app/startup.sh
