#!/usr/bin/env bash
set -e

# version and keys are supplied as arguments
version=${1}
rc=${version}
passphrase=$keypw
keyfile=$keyfile
if [ -z "$passphrase" ]; then
  echo passphrase is empty
else
  echo passphrase is not empty
fi
if [ -z "$keyfile" ]; then
  echo keyfile is empty
else
  echo keyfile is not empty
fi


rc=`echo ${version} | awk -F - '{print $2}'`
echo $keyfile | base64 -d | gpg --batch --passphrase $passphrase --import

function sign_bin {
  os=${1}
  arch=${2}
  pkg=${3}
  releasePkg=${pkg}-${version}-${os}-${arch}
  echo Signing ${releasePkg}...
  # set binary name
  bin=${pkg}
  if [ ${os} == "windows" ]; then
    bin=${bin}.exe
  elif [ ${os} == "darwin" ] && [ ${pkg} == "scp-webwallet" ]; then
    bin=${bin}.app
  fi
  # set folder
  folder=release/${releasePkg}
  rm -rf ${folder}
  mkdir -p ${folder}

  cp -r artifacts/${os}/${arch}/${bin} ${folder}
  
  # set path to bin
  binpath=${folder}/${bin}
  if [ ${os} == "darwin" ]; then
    binpath=${binpath}/Contents/MacOS/${pkg}
  fi

  chmod 755 ${binpath}

  if ! [[ -z ${keyfile} ]]; then
    echo $passphrase | PASSPHRASE=$passphrase gpg --batch --pinentry-mode loopback --command-fd 0 --armour --output  ${folder}/${bin}.asc --detach-sig ${binpath}
  fi
  sha1sum ${binpath} >> release/${pkg}-${version}-SHA1SUMS.txt
  cp -r LICENSE README.md ${folder}  
  (
    cd release/
    zip -rq ${releasePkg}.zip ${releasePkg}
  )
  if ! [[ -z ${keyfile} ]]; then
    echo $passphrase | PASSPHRASE=$passphrase gpg --batch --pinentry-mode loopback --command-fd 0 --armour --output  ${folder}.zip.asc --detach-sig ${folder}.zip
  fi
}

# Build the wallet.wasm resource
echo Building wallet.wasm
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ./resources/resources/wasm_exec.js
GOOS=js GOARCH=wasm go build -o ./resources/resources/wallet.wasm ./cmd/wasm/main.go

# Build the cold wallet
mkdir -p release
go run cmd/scp-cold-wallet/main.go > release/scp-cold-wallet.html

sign_bin linux amd64 scp-webwallet
sign_bin linux amd64 scp-webwallet-server

sign_bin windows amd64 scp-webwallet
sign_bin windows amd64 scp-webwallet-server

sign_bin darwin amd64 scp-webwallet
sign_bin darwin arm64 scp-webwallet
