#!/usr/bin/env bash


# Create fresh artifacts folder.
rm -rf  artifacts/darwin
mkdir -p artifacts/darwin

# Return first error encountered by any command.
set -e

echo Building wallet.wasm
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ./resources/resources/wasm_exec.js
GOOS=js GOARCH=wasm go build -o ./resources/resources/wallet.wasm ./cmd/wasm/main.go

WW_APP_ID=me.scpri.webwallet

echo building MacOS amd64
mkdir artifacts/darwin/amd64
for pkg in scp-webwallet scp-webwallet-server; do
    echo ${pkg}...
    CC=o64-clang fyne package -os darwin --src ./cmd/${pkg} --appID ${WW_APP_ID} --release --icon $(pwd)/resources/resources/icon.png
    mv ${pkg}.app artifacts/darwin/amd64
done

echo building MacOS arm64
mkdir artifacts/darwin/arm64
for pkg in scp-webwallet scp-webwallet-server; do
    CC=oa64-clang fyne package -os darwin --src ./cmd/${pkg} --appID ${WW_APP_ID} --release --icon $(pwd)/resources/resources/icon.png
    mv ${pkg}.app artifacts/darwin/arm64
done