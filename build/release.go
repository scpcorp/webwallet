package build

const (
	// ReleaseDev defines dev var value
	ReleaseDev = "dev"

	// ReleaseStandard defines standard var value
	ReleaseStandard = "standard"
)
