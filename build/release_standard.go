//go:build !dev

package build

const (
	// Release defines which type of release it is.
	Release = ReleaseStandard
)
