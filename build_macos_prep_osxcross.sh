#!/usr/bin/env bash

git clone https://github.com/tpoechtrager/osxcross.git

cd osxcross/
wget https://kirneh.eu/MacOSX13.3.sdk.tar.xz -O tarballs/MacOSX13.3.sdk.tar.xz

UNATTENDED=1  ./build.sh

cd ..