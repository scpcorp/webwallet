package jsonmanager

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/scpcorp/webwallet/build"
)

// JSONNotes represents the JSON format of the notes
type JSONNotes struct {
	TransactionID    string `json:"transactionID"`
	TransactionNotes string `json:"transactionNotes"`
}

// NotesManager represents the manager for the notes
type NotesManager struct {
	filePath string
	mutex    sync.Mutex
}

func (nm *NotesManager) getAll() ([]JSONNotes, error) {
	file, err := os.ReadFile(nm.filePath)
	if err != nil {
		return nil, err
	}
	var notes []JSONNotes
	err = json.Unmarshal(file, &notes)
	if err != nil {
		return nil, err
	}
	return notes, nil
}

// GetAll returns all the notes
func (nm *NotesManager) GetAll() ([]JSONNotes, error) {
	nm.mutex.Lock()
	defer nm.mutex.Unlock()
	return nm.getAll()
}

// UpsertNote updates or inserts the notes with the given transactionID and transactionNotes
func (nm *NotesManager) UpsertNote(transactionID string, transactionNotes string) error {
	newNote := JSONNotes{TransactionID: transactionID, TransactionNotes: transactionNotes}

	nm.mutex.Lock()
	defer nm.mutex.Unlock()

	notes, err := nm.getAll()
	if err != nil {
		return err
	}

	exists := false
	for i, note := range notes {
		if note.TransactionID == newNote.TransactionID {
			notes[i] = newNote
			exists = true
			break
		}
	}
	if !exists {
		notes = append(notes, newNote)
	}

	notesData, err := json.MarshalIndent(notes, "", "    ")
	if err != nil {
		return err
	}

	return os.WriteFile(nm.filePath, notesData, 0640)
}

// DeleteNote deletes the note with the given transactionID
func (nm *NotesManager) DeleteNote(transactionID string) error {
	nm.mutex.Lock()
	defer nm.mutex.Unlock()

	notes, err := nm.getAll()
	if err != nil {
		return err
	}

	for i, note := range notes {
		if note.TransactionID == transactionID {
			notes = append(notes[:i], notes[i+1:]...)
			notesData, err := json.MarshalIndent(notes, "", "    ")
			if err != nil {
				return err
			}
			return os.WriteFile(nm.filePath, notesData, 0640)
		}
	}
	return nil // No error if note doesn't exist
}

// FileExists returns whether the given file exists
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		// File does not exist
		return false
	}
	return !info.IsDir() // Return false if it's a directory, true if it's not
}

// NewNotesManager returns a new NotesManager for the given wallet
func NewNotesManager(walletName string) *NotesManager {
	basePath := build.ScPrimeWebWalletDir()
	finalFile := filepath.Join(basePath, "wallets", walletName, "notes.json")

	if !FileExists(finalFile) {
		file, _ := os.Create(finalFile)
		defer file.Close()
		io.WriteString(file, "[]")
	}

	return &NotesManager{filePath: finalFile}
}
