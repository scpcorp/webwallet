package server

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/scpcorp/webwallet/modules/bootstrapper"
	consensusbuilder "gitlab.com/scpcorp/webwallet/modules/consensesbuilder"
	jsonmanager "gitlab.com/scpcorp/webwallet/server/json"

	"github.com/mr-tron/base58"
	mnemonics "gitlab.com/NebulousLabs/entropy-mnemonics"
	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

type TransportHistoryRecord struct {
	BurnID  types.TransactionID         `json:"burn_id"`
	Status  types.SolanaTransportStatus `json:"status"`
	Amount  string                      `json:"currency"`
	Created types.Timestamp             `json:"created"`
}
type JSONMessage struct {
	Success bool        `json:"success"`
	Message interface{} `json:"message"`
}

func NewErrorMessage(message interface{}) JSONMessage {
	return JSONMessage{
		Success: false,
		Message: message,
	}
}

func NewSuccessMessage(message interface{}) JSONMessage {
	return JSONMessage{
		Success: true,
		Message: message,
	}
}

func sendMessage(w http.ResponseWriter, message JSONMessage) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	if message.Success {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	err := json.NewEncoder(w).Encode(message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func restUnlockWallet(w http.ResponseWriter, req *http.Request) {
	password := req.FormValue("password")
	walletName := req.FormValue("wallet")
	skipTransporterForm := req.FormValue("skip_transporter")

	skipTransporter = false
	if skipTransporterForm == "true" {
		skipTransporter = true
	}

	sessionID := addSessionID()
	wallet, err := existingWallet(walletName, sessionID)
	if err != nil {
		closeSessionWallet(wallet, sessionID)
		sendMessage(w, NewErrorMessage(fmt.Sprintf("Unable to unlock wallet: %v", err)))
		return
	}
	status = "Scanning"
	var msgPrefix = "Unable to unlock wallet: "
	if password == "" {
		closeSessionWallet(wallet, sessionID)
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s: A password must be provided.", msgPrefix)))
		return
	}
	potentialKeys, _ := encryptionKeys(password)
	for _, key := range potentialKeys {
		errChan := wallet.UnlockAsync(key)
		var unlockErr error
		select {
		case unlockErr = <-errChan:
		default:
		}
		if unlockErr == nil {
			status = ""
			sendMessage(w, NewSuccessMessage(map[string]interface{}{"unlocked": true}))
			return
		}
		err = errors.Join(err, unlockErr)
	}
	if err != nil {
		closeSessionWallet(wallet, sessionID)
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		if status == "Scanning" {
			status = ""
		}
		return
	}
}

func restLockWallet(w http.ResponseWriter, req *http.Request) {
	var msgPrefix = "Unable to lock wallet: "
	sessionID := req.FormValue("session_id")
	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	unlocked, err := wallet.Unlocked()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	if !unlocked {
		sendMessage(w, NewErrorMessage("Wallet was already locked."))
		return
	}
	wallet.Lock()
	closeWallet(sessionID)
	sendMessage(w, NewSuccessMessage("Wallet Locked"))
}

func restSessions(w http.ResponseWriter, req *http.Request) {
	type retSession struct {
		Name string
		ID   string
	}
	var ret []retSession

	for _, s := range sessions {
		if s != nil && s.name != "" {
			ret = append(ret, retSession{Name: s.name, ID: s.id})
		}
	}

	//json, err := json.Marshal(ret)

	if len(ret) == 0 {
		sendMessage(w, NewErrorMessage("No sessions active."))
		return
	}
	sendMessage(w, NewSuccessMessage(ret))
}

func restWallets(w http.ResponseWriter, req *http.Request) {
	ret, err := getWallets()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%v", err)))
		return
	}
	sendMessage(w, NewSuccessMessage(ret))
}

func restBlockHeight(w http.ResponseWriter, req *http.Request) {
	sessionID := req.URL.Query().Get("session_id")
	fmtHeight, fmtStatus, fmtStatCo := blockHeightHelper(sessionID)
	type BHInfo struct {
		Height      string
		Status      string
		StatusColor string
	}
	sendMessage(w, NewSuccessMessage(BHInfo{Height: fmtHeight, Status: fmtStatus, StatusColor: fmtStatCo}))
}

func restBalance(w http.ResponseWriter, req *http.Request) {
	sessionID := req.URL.Query().Get("session_id")
	sendMessage(w, NewSuccessMessage(v2BalancesHelper(sessionID)))
}

func restDeleteConsensus(w http.ResponseWriter, req *http.Request) {
	deleteConsensus := req.FormValue("delete_consensus")
	if deleteConsensus != "yes" {
		sendMessage(w, NewErrorMessage("Post form data 'delete_consensus' as 'yes' to delete"))
		return
	}
	consensusbuilder.Close()
	n.Close()
	bootstrapper.Close()

	if err := consensusbuilder.DeleteConsensusFile(config.Dir); err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("unable to delete consensus: %s", err.Error())))
		return
	}
	sendMessage(w, NewSuccessMessage("Consensus deleted, please restart web wallet."))
	if UI != nil {
		UI.Close()
	}

	srv.Close()

	self, err := os.Executable()
	if err != nil {
		return
	}
	args := os.Args
	env := os.Environ()
	// Windows does not support exec syscall.
	if runtime.GOOS == "windows" {
		cmd := exec.Command(self, args[1:]...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Stdin = os.Stdin
		cmd.Env = env
		err := cmd.Run()
		if err == nil {
			os.Exit(0)
		}
		return
	}
	syscall.Exec(self, args, env)
	os.Exit(0)
}

func restTransactions(w http.ResponseWriter, req *http.Request) {
	sessionID := req.URL.Query().Get("session_id")
	msgPrefix := "Unable to generate transaction history: "

	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}

	count := 0
	heightMin := 0
	confirmedTxns, err := wallet.Transactions(types.BlockHeight(heightMin), n.ConsensusSet.Height())
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	unconfirmedTxns, err := wallet.UnconfirmedTransactions()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	sts, err := ComputeSummarizedTransactions(append(confirmedTxns, unconfirmedTxns...), n.ConsensusSet.Height(), wallet)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	lines := []TransactionHistoryLine{}
	for i := len(sts) - 1; i >= 0; i-- {
		txn := sts[i]
		// Format transaction type
		isSetup := txn.Type == "SETUP" && txn.Scp == 0 && txn.SpfA == 0 && txn.SpfB == 0
		if !isSetup {
			count++
			var amountArr []string
			if txn.Scp != 0 {
				amountArr = append(amountArr, strings.TrimRight(strings.TrimRight(fmt.Sprintf("%15.4f", txn.Scp), "0"), ".")+" SCP")
			}
			if txn.SpfA != 0 {
				postfix := "SPF-A"
				if txn.Confirmed == _stUnconfirmedStr { // in case of unconfirmed we just show SPF
					postfix = "SPF"
				}
				amountArr = append(amountArr, fmt.Sprintf("%14v %s", txn.SpfA, postfix))
			}
			if txn.SpfB != 0 {
				amountArr = append(amountArr, fmt.Sprintf("%14v SPF-B", txn.SpfB))
			}
			fmtAmount := strings.Join(amountArr, "; ")
			if fmtAmount == "" {
				fmtAmount = "0 SCP/SPF"
			}
			var fmtFee string
			if txn.ScpFee != 0 {
				fmtFee = fmt.Sprintf("%f SCP fee", txn.ScpFee)
			}
			line := TransactionHistoryLine{}
			line.TransactionID = txn.TxnID
			line.ShortTransactionID = txn.TxnID[0:16] + "..." + txn.TxnID[len(txn.TxnID)-16:]
			line.Type = txn.Type
			line.Time = txn.Time
			line.AmountSCP = txn.Scp
			line.AmountSPFA = txn.SpfA
			line.AmountSPFB = txn.SpfB
			line.Amount = fmtAmount
			line.Fee = fmtFee
			line.FeeSCP = txn.ScpFee
			line.Confirmed = txn.Confirmed
			lines = append(lines, line)
		}
	}
	sendMessage(w, NewSuccessMessage(lines))
}

func restTransactionDetails(w http.ResponseWriter, req *http.Request) {
	sessionID := req.URL.Query().Get("session_id")
	txId := req.FormValue("transaction_id")
	var msgPrefix = "Unable to retrieve the transaction: "

	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}
	if txId == "" {
		sendMessage(w, NewErrorMessage("No transaction ID ('transaction_id') provided."))
		return
	}
	var transactionID types.TransactionID
	err := transactionID.UnmarshalJSON([]byte("\"" + txId + "\""))
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, "Unable to parse transaction ID.")))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	txn, ok, err := wallet.Transaction(transactionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	if !ok {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, "Transaction was not found.")))
		return
	}
	transactionDetails, _ := v2TransactionDetailsHelper(txn)
	sendMessage(w, NewSuccessMessage(transactionDetails))
}

func restAddresses(w http.ResponseWriter, req *http.Request) {
	sessionID := req.URL.Query().Get("session_id")
	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}

	var getNewAddress bool
	sNewAddress := req.URL.Query().Get("new_address")
	if sNewAddress == "yes" {
		getNewAddress = true
	}

	var msgPrefix = "Unable to retrieve address: "
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	addresses, err := wallet.LastAddresses(10)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	if len(addresses) == 0 {
		_, err := wallet.NextAddress()
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
			return
		}
	}

	type AddressList struct {
		NewAddress string
		Addresses  []string
	}
	var ret AddressList

	var newAddr types.UnlockConditions
	if getNewAddress {
		newAddr, err = wallet.NextAddress()
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
			return
		}
		ret.NewAddress = newAddr.UnlockHash().String()
	}
	addresses, err = wallet.LastAddresses(10)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}

	for _, v := range addresses {
		ret.Addresses = append(ret.Addresses, v.String())
	}
	sendMessage(w, NewSuccessMessage(ret))
}

func restInitializeSeed(w http.ResponseWriter, req *http.Request) {
	walletDirName := req.FormValue("wallet_dir_name")
	if walletDirName == "" {
		walletDirName = "wallet"
	}
	newPassword := req.FormValue("new_password")
	confirmPassword := req.FormValue("confirm_password")

	skipTransporterForm := req.FormValue("skip_transporter")

	skipTransporter = false
	if skipTransporterForm == "true" {
		skipTransporter = true
	}

	var msgPrefix = "Unable to initialize new wallet seed: "
	if newPassword == "" {
		msg := "A new password must be provided."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	if len(newPassword) < 8 {
		msg := "Password must be at least eight characters long."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	if confirmPassword == "" {
		msg := "A confirmation password must be provided."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	if newPassword != confirmPassword {
		msg := "New password does not match confirmation password."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	sessionID := addSessionID()
	wallet, err := newWallet(walletDirName, sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	encrypted, err := wallet.Encrypted()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	if encrypted {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	go initializeSeedHelper(newPassword, sessionID)
	sendMessage(w, NewSuccessMessage(map[string]interface{}{"initializing": true}))
}

func restRestoreSeed(w http.ResponseWriter, req *http.Request) {
	walletDirName := req.FormValue("wallet")
	newPassword := req.FormValue("new_password")
	confirmPassword := req.FormValue("confirm_password")
	seedStr := req.FormValue("seed_str")
	skipTransporterForm := req.FormValue("skip_transporter")

	skipTransporter = false
	if skipTransporterForm == "true" {
		skipTransporter = true
	}

	var msgPrefix = "Unable to restore wallet from seed: "
	if newPassword == "" {
		msg := "A new password must be provided."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	if confirmPassword == "" {
		msg := "A confirmation password must be provided."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	if newPassword != confirmPassword {
		msg := "New password does not match confirmation password."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	if seedStr == "" {
		msg := "A seed must be provided."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	sessionID := addSessionID()
	wallet, err := newWallet(walletDirName, sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	encrypted, err := wallet.Encrypted()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	if encrypted {
		msg := "Seed is already initialized."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, msg)))
		return
	}
	seed, err := modules.StringToSeed(seedStr, "english")
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", msgPrefix, err)))
		return
	}
	go restoreSeedHelper(newPassword, seed, sessionID)

	sendMessage(w, NewSuccessMessage(map[string]interface{}{"restoring": true, "sessionId": sessionID, "walletName": walletDirName}))
}

func restHeaderData(w http.ResponseWriter, req *http.Request) {
	sessionID := req.URL.Query().Get("session_id")
	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}

	type BHInfo struct {
		Height      string
		Status      string
		StatusColor string
	}
	type HeaderData struct {
		Balance interface{}
		Height  BHInfo
	}
	balance := v2BalancesHelper(sessionID)
	fmtHeight, fmtStatus, fmtStatCo := blockHeightHelper(sessionID)
	bh := BHInfo{Height: fmtHeight, Status: fmtStatus, StatusColor: fmtStatCo}

	sendMessage(w, NewSuccessMessage(HeaderData{Balance: balance, Height: bh}))
}

func restSendCoins(w http.ResponseWriter, req *http.Request) {
	sessionID := req.FormValue("session_id")
	destinationAddress := req.FormValue("destination")
	destinationAmount := req.FormValue("amount")
	coinType := req.FormValue("coin_type")

	errorPrefix := "Unable to send coins: "
	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	unlocked, err := wallet.Unlocked()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	if !unlocked {
		msg := "Wallet is locked."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, msg)))
		return
	}
	dest, err := scanAddress(destinationAddress)
	if err != nil {
		msg := "Destination is not valid."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, msg)))
		return
	}
	switch coinType {
	case "SCP":
		amount, err := NewCurrencyStr(destinationAmount + "SCP")
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		_, err = wallet.SendSiacoins(amount, dest)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
	case "SPFA":
		amount, err := NewCurrencyStr(destinationAmount + "SPF")
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		_, err = wallet.SendSiafunds(amount, dest)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
	case "SPFB":
		amount, err := NewCurrencyStr(destinationAmount + "SPF")
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		_, err = wallet.SendSiafundbs(amount, dest)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
	default:
		msg := "Coin type was not supplied."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, msg)))
		return
	}
	sendMessage(w, NewSuccessMessage("Coin send successful."))
}

func restMultiSendCsv(w http.ResponseWriter, req *http.Request) {
	errorPrefix := "Unable to send coins: "

	if err := req.ParseMultipartForm(10 << 20); err != nil { // 10 MB
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}

	sessionID := req.FormValue("session_id")
	coinType := req.FormValue("coin_type")

	if coinType != "SCP" && coinType != "SPF-A" && coinType != "SPF-B" {
		coinType = "SCP"
	}

	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}

	file, _, err := req.FormFile("file")
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	lines, err := csv.NewReader(file).ReadAll()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	err = file.Close()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%s%v", errorPrefix, "Failed to close the multisend csv upload stream: ", err)))
		return
	}
	var coinOutputs []types.SiacoinOutput
	var fundAOutputs []types.SiafundOutput
	var fundBOutputs []types.SiafundOutput
	for _, line := range lines {
		if line == nil {
			continue
		}
		amount := line[0]
		if amount == "" {
			continue
		}
		dest := line[1]
		if dest == "" {
			continue
		}
		//add default coin type if there is none (verified by successful string parse into float)
		if _, err := strconv.ParseFloat(amount, 64); err == nil {
			amount += coinType
		}
		// SPF-A and SPF-B are not recognised as currency units, so just "SPF" should be supplied
		// and different methods called for transfer
		value, err := types.NewCurrencyStr(strings.ReplaceAll(strings.ReplaceAll(amount, "SPF-A", "SPF"), "SPF-B", "SPF"))
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%s%v", errorPrefix, "Could not parse amount: ", err)))
			return
		}
		var hash types.UnlockHash
		if _, err := fmt.Sscan(dest, &hash); err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%s%v", errorPrefix, "Failed to parse destination address: ", err)))
			return
		}
		if strings.HasSuffix(amount, "SPF") || strings.HasSuffix(amount, "SPF-A") {
			var output types.SiafundOutput
			output.Value = value
			output.UnlockHash = hash
			fundAOutputs = append(fundAOutputs, output)
		} else if strings.HasSuffix(amount, "SPF-B") {
			var output types.SiafundOutput
			output.Value = value
			output.UnlockHash = hash
			fundBOutputs = append(fundBOutputs, output)
		} else {
			var output types.SiacoinOutput
			output.Value = value
			output.UnlockHash = hash
			coinOutputs = append(coinOutputs, output)
		}
	}
	if len(coinOutputs) == 0 && len(fundAOutputs) == 0 && len(fundBOutputs) == 0 {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%s%v", errorPrefix, "No ScPrime outputs were supplied: ", err)))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	unlocked, err := wallet.Unlocked()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	if !unlocked {
		msg := "Wallet is locked."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, msg)))
		return
	}
	// Mock the transaction to verify that the wallet is able to fund all transactions.
	// SPF-A and SPF-B cannot be sent in same transaction
	txnBuilder, err := wallet.BuildUnsignedBatchTransaction(coinOutputs, fundAOutputs, nil)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	txnBuilder.Drop()
	// test SCP+SPF-B
	txnBuilder, err = wallet.BuildUnsignedBatchTransaction(coinOutputs, nil, fundBOutputs)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	txnBuilder.Drop()
	// Send the transactions
	if len(coinOutputs) != 0 {
		_, err = wallet.SendBatchTransaction(coinOutputs, nil, nil)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
	}
	if len(fundAOutputs) != 0 {
		_, err = wallet.SendBatchTransaction(nil, fundAOutputs, nil)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
	}
	if len(fundBOutputs) != 0 {
		_, err = wallet.SendBatchTransaction(nil, nil, fundBOutputs)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
	}
	sendMessage(w, NewSuccessMessage("Coin send successful."))
}

func restChangeWalletPassword(w http.ResponseWriter, req *http.Request) {
	sessionID := req.FormValue("session_id")
	origPassword := req.FormValue("orig_password")
	newPassword := req.FormValue("new_password")
	errorPrefix := "Unable to change lock: "

	if origPassword == "" {
		err := "The original password must be provided."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	if newPassword == "" {
		err := "A new password must be provided."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	if len(newPassword) < 8 {
		err := "Password must be at least eight characters long."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	validPass, err := isPasswordValid(wallet, origPassword)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	} else if !validPass {
		err := "The original password is not valid."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	newKey := crypto.NewWalletKey(crypto.HashObject(newPassword))
	primarySeed, _, _ := wallet.PrimarySeed()
	err = wallet.ChangeKeyWithSeed(primarySeed, newKey)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	sendMessage(w, NewSuccessMessage("Password change successful"))
}

func restRecoverSeed(w http.ResponseWriter, req *http.Request) {
	sessionID := req.URL.Query().Get("session_id")
	errorPrefix := "Unable to recover seed: "

	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}

	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	unlocked, err := wallet.Unlocked()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	if !unlocked {
		msg := "Wallet is locked."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, msg)))
		return
	}

	// Get the primary seed information.
	dictionary := mnemonics.DictionaryID(req.URL.Query().Get("dictionary"))
	if dictionary == "" {
		dictionary = mnemonics.English
	}
	primarySeed, _, err := wallet.PrimarySeed()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	primarySeedStr, err := modules.SeedToString(primarySeed, dictionary)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}

	sendMessage(w, NewSuccessMessage(map[string]interface{}{"seed": primarySeedStr}))
}

func restConsensusStatus(w http.ResponseWriter, req *http.Request) {
	type ConsensusState struct {
		Builder      interface{}
		Bootstrapper interface{}
	}
	//sendMessage(w, NewSuccessMessage(consensusbuilder.Progress()))
	sendMessage(w, NewSuccessMessage(ConsensusState{Builder: consensusbuilder.Progress(), Bootstrapper: bootstrapper.Progress()}))
}

func restConsensusBuild(w http.ResponseWriter, req *http.Request) {
	buildType := req.FormValue("build_type")
	switch buildType {
	case "bootstrap":
		bootstrapper.Initialize()
		break
	case "build":
		bootstrapper.Skip()
		time.Sleep(50 * time.Millisecond)
		consensusbuilder.Initialize()
		break
	case "upload":
		break
	}

	type ConsensusState struct {
		Builder      interface{}
		Bootstrapper interface{}
	}
	sendMessage(w, NewSuccessMessage(ConsensusState{
		Builder:      consensusbuilder.Progress(),
		Bootstrapper: bootstrapper.Progress(),
	}))
}

func restTransactionHistoryCsvExport(w http.ResponseWriter, req *http.Request) {
	sessionID := req.FormValue("session_id")
	errorPrefix := "Unable to export transaction history: "

	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	unlocked, err := wallet.Unlocked()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	if !unlocked {
		msg := "Wallet is locked."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, msg)))
		return
	}
	history, err := transctionHistoryCsvExportHelper(wallet)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	sendMessage(w, NewSuccessMessage(history))
}

//func testAllowanceResponse() (SpfTransportAllowance, error) {
//	tempAddress1, _ := scanAddress("76a74825e3bde39af8865e9cab632bb1c2f1fb020b605aff22a60e8a627bf70ae726a14bf5e4")
//	tempAddress2, _ := scanAddress("0c4832d127d3a80ee7e4417c69b42013a73f1fca661d475b9726efbb1e1d250bcc4135544b60")
//	tempCurrency1, _ := NewCurrencyStr("300000SPF")
//	tempCurrency2, _ := NewCurrencyStr("950000SPF")
//	tempCurrency3, _ := NewCurrencyStr("2000SPF")
//	tempCurrency4, _ := NewCurrencyStr("5000SPF")
//	premine := make(map[types.UnlockHash]SpfTransportTypeAllowance)
//	premine[tempAddress1] = SpfTransportTypeAllowance{MaxAllowed: tempCurrency1, WaitTime: 1708260430000}
//	premine[tempAddress2] = SpfTransportTypeAllowance{MaxAllowed: tempCurrency2, WaitTime: 1708260430000}
//
//	return SpfTransportAllowance{
//		Regular:  SpfTransportTypeAllowance{MaxAllowed: tempCurrency3, WaitTime: 1708260430000},
//		Premined: premine,
//		//Airdrop: nil,
//		Airdrop: &SpfTransportTypeAllowance{MaxAllowed: tempCurrency4, WaitTime: 1708260430000},
//	}, nil
//}

//func testHistory() ([]types.SpfTransport, error) {
//	tempCurrency1, _ := NewCurrencyStr("50SPF")
//	tempCurrency2, _ := NewCurrencyStr("130SPF")
//	tempHistory := make([]types.SpfTransport, 2)
//	tempHistory[0] = types.SpfTransport{
//		SpfTransportRecord: types.SpfTransportRecord{Status: types.BurnBroadcasted, Amount: tempCurrency1, Created: time.Now()},
//	}
//	tempHistory[1] = types.SpfTransport{
//		SpfTransportRecord: types.SpfTransportRecord{Status: types.BurnBroadcasted, Amount: tempCurrency2, Created: time.Now()},
//	}
//
//	//tempHistory := make([]types.SpfTransportRecord, 2)
//	//tempHistory[0] = types.SpfTransportRecord{Status: types.BurnBroadcasted, Amount: tempCurrency1, Created: time.Now()}
//	//tempHistory[1] = types.SpfTransportRecord{Status: types.SubmittedToTransporter, Amount: tempCurrency2, Created: time.Now()}
//	return tempHistory, nil
//}

type AllowanceResponse struct {
	Regular  types.SolanaTransportAllowance            `json:"regular"`
	Premined map[string]types.SolanaTransportAllowance `json:"premined"`
	Airdrop  *types.SolanaTransportAllowance           `json:"airdrop,omitempty"`
	History  []TransportHistoryRecord                  `json:"history"`
	Balance  interface{}                               `json:"balance"`
	CoinType string                                    `json:"coinType"`
}

func restTransporterAllowance(w http.ResponseWriter, req *http.Request) {
	sessionID := req.FormValue("session_id")
	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}
	coinType := req.FormValue("coin_type")

	errorPrefix := "Unable to process Transporter allowance: "

	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}

	var response AllowanceResponse
	if coinType == "SPFA" || coinType == "SPFB" {
		currencyType := types.SpfA
		if coinType == "SPFB" {
			currencyType = types.SpfB
		}
		spfAllowance, err := wallet.SiafundTransportAllowance(currencyType)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		response.Regular = spfAllowance.Regular
		response.Premined = make(map[string]types.SolanaTransportAllowance)
		for key, value := range spfAllowance.Premined {
			response.Premined[key] = value
		}
		response.Airdrop = spfAllowance.Airdrop
	} else {
		scpAllowance, err := wallet.ScpTransportAllowance()
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		response.Regular = *scpAllowance
	}

	scpHistory, err := wallet.ScpTransportHistory()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	spfHistory, err := wallet.SiafundTransportHistory()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}

	response.History = append(currencyHistoryToStrHistory(scpHistory, types.Scp), currencyHistoryToStrHistory(spfHistory, types.Spf)...)
	response.Balance = v2BalancesHelper(sessionID)
	response.CoinType = coinType

	sendMessage(w, NewSuccessMessage(response))
}

func restTransporterSend(w http.ResponseWriter, req *http.Request) {
	sessionID := req.FormValue("session_id")
	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}

	errorPrefix := "Unable to transport coins: "

	amount := req.FormValue("amount")
	coinType := req.FormValue("coin_type")

	transportType := types.Regular
	switch req.FormValue("transport_type") {
	case "airdrop":
		transportType = types.Airdrop
	case "premined":
		transportType = types.Premined
	}

	var unlockHash *types.UnlockHash = nil
	if transportType != types.Regular {
		uHash, err := scanAddress(req.FormValue("unlock_hash"))
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		unlockHash = &uHash
	}
	solanaAddr, err := BuildSolanaAddress(req.FormValue("solanaAddr"))
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}

	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}

	var duration time.Duration
	var currency *types.Currency
	if coinType == "SPFA" || coinType == "SPFB" {
		currencyType := types.SpfA
		if coinType == "SPFB" {
			currencyType = types.SpfB
		}
		currencyAmount := new(big.Int)
		_, ok := currencyAmount.SetString(amount, 10)
		if !ok {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, "Could not convert amount")))
			return
		}
		finalAmount := types.SpfAmount{Amount: types.NewCurrency(currencyAmount), Type: currencyType}
		duration, currency, err = wallet.SiafundTransportSend(finalAmount, transportType, unlockHash, solanaAddr)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
	} else {
		finalAmount, err := NewCurrencyStr(amount + "SCP")
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}

		//divide and multiply a tmp var to clear out post-nano digits. If it'll match original amount, then resolution is ok
		fBig := finalAmount.Big()
		tmpAmount := new(big.Int).Set(fBig)
		tmpAmount.Div(tmpAmount, new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil))
		tmpAmount.Mul(tmpAmount, new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil))
		if tmpAmount.Cmp(fBig) != 0 {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, "Transporting supported up to nano SCP resolution (0.000000001). Decrease your decimal places to transport")))
			return
		}

		duration, err = wallet.ScpTransportSend(finalAmount, solanaAddr)
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
	}

	type TransportResponse struct {
		Duration time.Duration   `json:"duration"`
		Currency *types.Currency `json:"currency"`
	}
	sendMessage(w, NewSuccessMessage(TransportResponse{Duration: duration, Currency: currency}))
}

func BuildSolanaAddress(encodedString string) (types.SolanaAddress, error) {
	decoded, err := base58.Decode(encodedString)
	if err != nil {
		return types.SolanaAddress{}, err
	}
	if len(decoded) != types.SolanaAddrLen {
		return types.SolanaAddress{}, fmt.Errorf("invalid length: expected %d bytes, got %d", types.SolanaAddrLen, len(decoded))
	}

	var addr types.SolanaAddress
	copy(addr[:], decoded)
	return addr, nil
}

func restNotesJSON(w http.ResponseWriter, req *http.Request) {
	walletName := req.FormValue("wallet")
	errorPrefix := "Unable to access notes file: "
	manager := jsonmanager.NewNotesManager(walletName)
	//fmt.Printf("restNotesJSON called with method: %s and wallet name of %s", req.Method, walletName)

	switch req.Method {
	case http.MethodGet: // Get records from the JSON array
		notes, err := manager.GetAll()
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		sendMessage(w, NewSuccessMessage(notes))
	case http.MethodPost: // Add a new record to the JSON array
		err := manager.UpsertNote(req.FormValue("transactionId"), req.FormValue("transactionNotes"))
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		sendMessage(w, NewSuccessMessage("Notes updated"))
	case http.MethodPut: // Update a record in the JSON array
		err := manager.UpsertNote(req.FormValue("transactionId"), req.FormValue("transactionNotes"))
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		sendMessage(w, NewSuccessMessage("Notes updated"))
	case http.MethodDelete: // Delete a record from the JSON array
		err := manager.DeleteNote(req.FormValue("transactionId"))
		if err != nil {
			sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
			return
		}
		sendMessage(w, NewSuccessMessage("Note removed"))
	default:
		sendMessage(w, NewErrorMessage("Unknown method type: "+req.Method))
	}
}

func currencyHistoryToStrHistory(spdHistory []types.SolanaTransport, curType types.CurrencyType) []TransportHistoryRecord {
	ret := make([]TransportHistoryRecord, len(spdHistory))
	for i, r := range spdHistory {
		ret[i] = TransportHistoryRecord{
			BurnID:  r.BurnID,
			Status:  r.Status,
			Amount:  r.Amount.String(),
			Created: r.Created,
		}
		if curType == types.Scp {
			scpF, _ := new(big.Rat).SetFrac(r.Amount.Big(), types.ScPrimecoinPrecision.Big()).Float64()
			ret[i].Amount = strconv.FormatFloat(scpF, 'f', -1, 64) + " SCP"
		} else {
			ret[i].Amount += " SPF"
		}
	}

	return ret
}
