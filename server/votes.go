package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"time"

	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

const (
	eligibleHashesURL = "https://gitlab.com/scpcorp/community-governance/-/raw/main/eligible_hashes.json?inline=false"
	voteOptionsURL    = "https://gitlab.com/scpcorp/community-governance/-/raw/main/votes.json?inline=false"
)

type Vote struct {
	StartDate      time.Time    `json:"start_date"`
	EndDate        time.Time    `json:"end_date"`
	Title          string       `json:"title"`
	Description    string       `json:"description"`
	DescriptionURL string       `json:"description_url"`
	VotedFor       string       `json:"voted_for"`
	VoteOptions    []VoteOption `json:"vote_options"`
}
type VoteOption struct {
	Description string `json:"description"`
	VoteAddress string `json:"vote_address"`
}

type voteRet struct {
	Eligible bool   `json:"eligible"`
	Votes    []Vote `json:"votes"`
}

type outputTransaction struct {
	Output string    `json:"output"`
	Date   time.Time `json:"date"`
}

func restGetVoteOptions(w http.ResponseWriter, req *http.Request) {
	sessionID := req.URL.Query().Get("session_id")

	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error getting wallet: %s", err.Error())))
		return
	}

	var ret voteRet
	ret.Eligible, err = eligibleToVote(wallet)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error getting vote eligibility: %s", err.Error())))
		return
	}

	ret.Votes, err = getVoteOptions()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error getting vote options: %s", err.Error())))
	}

	err = fillVoted(&ret.Votes, wallet)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error filling vote info: %s", err.Error())))
	}

	msg := NewSuccessMessage(ret)
	if err != nil {
		msg = NewErrorMessage(fmt.Sprintf("error getting vote options: %s", err.Error()))
	}
	sendMessage(w, msg)
}

func restCastVote(w http.ResponseWriter, req *http.Request) {
	sessionID := req.FormValue("session_id")
	voteFor := req.FormValue("vote_for")

	errorPrefix := "Unable to cast vote: "
	if sessionID == "" || !sessionIDExists(sessionID) {
		sendMessage(w, NewErrorMessage("Session ID does not exist."))
		return
	}
	wallet, err := getWallet(sessionID)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	unlocked, err := wallet.Unlocked()
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, err)))
		return
	}
	if !unlocked {
		msg := "Wallet is locked."
		sendMessage(w, NewErrorMessage(fmt.Sprintf("%s%v", errorPrefix, msg)))
		return
	}

	sendFrom, err := getMyEligibleAddress(wallet)
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error getting eligible vote address: %s", err.Error())))
		return
	}
	if sendFrom == "" {
		sendMessage(w, NewErrorMessage("Cannot cast vote - no whitelisted addresses for this wallet"))
		return
	}

	amount, err := NewCurrencyStr("1H")
	if err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error forming send currency: %s", err.Error())))
		return
	}

	var dst, sender types.UnlockHash
	if err := dst.LoadString(voteFor); err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error forming sendTo hash: %s", err.Error())))
		return
	}
	if err := sender.LoadString(sendFrom); err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error forming sendFrom hash: %s", err.Error())))
		return
	}

	sendOutput := types.SiacoinOutput{
		Value:      amount,
		UnlockHash: dst,
	}
	if _, err := wallet.SendSiacoinsFromAddress([]types.SiacoinOutput{sendOutput}, sender); err != nil {
		sendMessage(w, NewErrorMessage(fmt.Sprintf("error sending vote: %s", err.Error())))
		return
	}
	sendMessage(w, NewSuccessMessage("Vote sent successfully"))
}

func fillVoted(votes *[]Vote, wallet modules.Wallet) error {
	earliestStart := time.Date(9999, 12, 31, 0, 0, 0, 0, time.UTC)
	latestEnd := time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC)

	for _, vote := range *votes {
		if vote.StartDate.Before(earliestStart) {
			earliestStart = vote.StartDate
		}
		if vote.EndDate.After(latestEnd) {
			latestEnd = vote.EndDate
		}
	}
	outputs, err := outputsInTimeRange(wallet, earliestStart, latestEnd)
	if err != nil {
		return err
	}
	for idx, vote := range *votes {
		for _, voteOption := range vote.VoteOptions {
			for _, output := range outputs {
				if output.Output == voteOption.VoteAddress && output.Date.After(vote.StartDate) && output.Date.Before(vote.EndDate) {
					(*votes)[idx].VotedFor = output.Output
				}
			}
		}
	}
	return nil
}

func gatherOutputs(txs []modules.ProcessedTransaction, start, end time.Time) []outputTransaction {
	var ret []outputTransaction
	for _, tx := range txs {
		rDate := time.Now()
		if uint64(tx.ConfirmationTimestamp) != unconfirmedTransactionTimestamp {
			rDate = time.Unix(int64(tx.ConfirmationTimestamp), 0)
		}
		if rDate.After(start) && rDate.Before(end) {
			for _, output := range tx.Outputs {
				r := outputTransaction{
					Date:   rDate,
					Output: output.RelatedAddress.String(),
				}
				ret = append(ret, r)
			}
		}
	}
	return ret
}

func outputsInTimeRange(wallet modules.Wallet, start, end time.Time) ([]outputTransaction, error) {
	var ret []outputTransaction

	confirmedTxns, err := wallet.Transactions(types.BlockHeight(0), n.ConsensusSet.Height())
	if err != nil {
		return nil, err
	}
	ret = append(ret, gatherOutputs(confirmedTxns, start, end)...)

	unconfirmedTxns, err := wallet.UnconfirmedTransactions()
	if err != nil {
		return nil, err
	}
	ret = append(ret, gatherOutputs(unconfirmedTxns, start, end)...)
	return ret, nil
}

func getVoteOptions() ([]Vote, error) {
	var ret, votes []Vote
	resp, err := http.Get(voteOptionsURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&votes); err != nil {
		return nil, err
	}

	for _, vote := range votes {
		if vote.StartDate.Before(time.Now()) && vote.EndDate.After(time.Now()) {
			ret = append(ret, vote)
		}
	}
	return ret, nil
}

func eligibleToVote(wallet modules.Wallet) (bool, error) {
	if addr, err := getMyEligibleAddress(wallet); err != nil {
		return false, err
	} else if addr != "" {
		return true, nil
	}
	return false, nil
}

func getMyEligibleAddress(wallet modules.Wallet) (string, error) {
	var myStringAddresses []string
	if myAddresses, err := wallet.AllAddresses(); err != nil {
		return "", err
	} else {
		for _, addr := range myAddresses {
			myStringAddresses = append(myStringAddresses, addr.String())
		}
		sort.Strings(myStringAddresses)
	}
	eligibleAddresses, err := getEligibleAddresses()
	if err != nil {
		return "", err
	}
	for _, addr := range myStringAddresses {
		for _, eligibleAddr := range eligibleAddresses {
			if addr == eligibleAddr {
				return addr, nil
			}
		}
	}
	return "", nil
}

func getEligibleAddresses() ([]string, error) {
	var ret []string
	resp, err := http.Get(eligibleHashesURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&ret); err != nil {
		return nil, err
	}

	return ret, nil
}
