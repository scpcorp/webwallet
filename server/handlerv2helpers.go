package server

import (
	"errors"
	"fmt"
	"math/big"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

type BalInfo struct {
	SCP            float64
	SCPUnconfirmed float64
	SPFA           uint64
	SPFB           uint64
	Claim          float64
	Whale          string
}

type TxInput struct {
	Value    types.Currency
	Address  types.UnlockHash
	FundType string
}
type TxOutput struct {
	Value    types.Currency
	Address  types.UnlockHash
	FundType string
}
type TxDetails struct {
	Timestamp types.Timestamp
	TxID      types.TransactionID
	TxType    modules.TXType
	TxBlock   types.BlockHeight
	Inputs    []TxInput
	Outputs   []TxOutput
}

type RestWallet struct {
	Name         string
	LastModified time.Time
}

func closeSessionWallet(wallet modules.Wallet, sessionID string) {
	if wallet != nil {
		wallet.Close()
	}
	if session, err := getSession(sessionID); err == nil && session != nil {
		session.wallet = nil
		session.name = ""
	}
}

func v2BalancesHelper(sessionID string) BalInfo {
	var ret BalInfo
	wallet, _ := getWallet(sessionID)
	if wallet == nil {
		return ret
	}
	unlocked, err := wallet.Unlocked()
	if err != nil {
		fmt.Printf("Unable to determine if wallet is unlocked: %v", err)
	}
	if unlocked {
		allBals, err := wallet.ConfirmedBalance()
		if err != nil {
			fmt.Printf("Unable to obtain confirmed balance: %v", err)
		} else {
			scpBal := allBals.CoinBalance
			fundABal := allBals.FundBalance
			fundBBal := allBals.FundbBalance
			// fundbBBal := allBals.FundbBalance
			claimBal := allBals.ClaimBalance
			// claimbBal := allBals.ClaimbBalance
			ret.SCP, _ = new(big.Rat).SetFrac(scpBal.Big(), types.ScPrimecoinPrecision.Big()).Float64()
			ret.Claim, _ = new(big.Rat).SetFrac(claimBal.Big(), types.ScPrimecoinPrecision.Big()).Float64()

			ret.SPFA, _ = fundABal.Uint64()
			ret.SPFB, _ = fundBBal.Uint64()
			ret.Whale = whaleHelper(ret.SCP)
		}
		scpOut, scpIn, err := wallet.UnconfirmedBalance()
		if err != nil {
			fmt.Printf("Unable to obtain unconfirmed balance: %v", err)
		} else {
			scpInFloat, _ := new(big.Rat).SetFrac(scpIn.Big(), types.ScPrimecoinPrecision.Big()).Float64()
			scpOutFloat, _ := new(big.Rat).SetFrac(scpOut.Big(), types.ScPrimecoinPrecision.Big()).Float64()
			ret.SCPUnconfirmed = scpInFloat - scpOutFloat
		}
	}
	return ret
}

func v2TransactionDetailsHelper(txn modules.ProcessedTransaction) (TxDetails, error) {
	var ret TxDetails

	ret.Timestamp = txn.ConfirmationTimestamp
	ret.TxID = txn.TransactionID
	ret.TxType = txn.TxType
	ret.TxBlock = txn.ConfirmationHeight

	for _, input := range txn.Inputs {
		var row TxInput
		row.Value = input.Value
		row.Address = input.RelatedAddress

		fmtFundType := strings.ToUpper(strings.Replace(fmt.Sprintf("%v", input.FundType), "_", " ", -1))
		fmtFundType = strings.Replace(fmtFundType, "SIACOIN", "SCP", -1)
		fmtFundType = strings.Replace(fmtFundType, "SIAFUND", "SPF", -1)
		row.FundType = fmtFundType

		ret.Inputs = append(ret.Inputs, row)
	}

	for _, output := range txn.Outputs {
		var row TxOutput
		row.Value = output.Value
		row.Address = output.RelatedAddress

		fmtFundType := strings.ToUpper(strings.Replace(fmt.Sprintf("%v", output.FundType), "_", " ", -1))
		fmtFundType = strings.Replace(fmtFundType, "SIACOIN", "SCP", -1)
		fmtFundType = strings.Replace(fmtFundType, "SIAFUND", "SPF", -1)
		row.FundType = fmtFundType

		ret.Outputs = append(ret.Outputs, row)
	}
	return ret, nil
}

// getWallets lists all the wallets by checking which wallet files exist
func getWallets() ([]RestWallet, error) {
	var ret []RestWallet

	walletDir := filepath.Join(n.Dir, "wallets")
	_, err := os.Stat(walletDir)
	if errors.Is(err, os.ErrNotExist) {
		return ret, nil
	}

	// Read the wallet directory
	files, err := os.ReadDir(walletDir)
	if err != nil {
		return nil, err
	}

	for _, f := range files {
		if !f.IsDir() {
			continue
		}

		walletDBPath := filepath.Join(walletDir, f.Name(), "wallet.db")
		info, err := os.Stat(walletDBPath)
		switch {
		case errors.Is(err, os.ErrNotExist):
			// The wallet.db file does not exist in this directory. Skip it.
		case err != nil:
			// Another error occurred while trying to stat the wallet.db file
			return nil, err
		default:
			ret = append(ret, RestWallet{Name: f.Name(), LastModified: info.ModTime()})
		}
	}
	return ret, nil
}
