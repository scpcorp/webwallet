package server

import (
	"fmt"
	"io/fs"
	"net/http"
	"time"

	chi "github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/scpcorp/webwallet/frontend"
)

func allowCORS(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*") // Consider restricting this to your front-end's domain
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Disposition")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next(w, r)
	}
}

func buildHTTPRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.Logger)
	// router.NotFound = http.HandlerFunc(notFoundHandler)
	// router.RedirectTrailingSlash = false

	//GUI Calls
	router.Get("/gui/bootstrapperProgress", bootstrapperProgressHandler)
	router.Get("/gui/consensusBuilderProgress", consensusBuilderProgressHandler)
	router.Get("/gui/logo.png", logoHandler)
	router.Get("/gui/scripts.js", scriptHandler)
	router.Get("/gui/wasm_exec.js", wasmExecHandler)
	router.Get("/gui/wallet.wasm", walletWasmHandler)
	router.Get("/gui/styles.css", styleHandler)
	router.Get("/initializeColdWallet", coldWalletHandler)

	if n == nil {
		router.Get("/initializeBootstrapper", initializeBootstrapperHandler)
		router.Get("/skipBootstrapper", skipBootstrapperHandler)
		router.Get("/initializeConsensusBuilder", initializeConsensusBuilderHandler)
		router.Get("/uploadConsensusSetForm", uploadConsensusSetFormHandler)
		router.Post("/uploadConsensusSet", uploadConsensusSetHandler)
	} else {

		router.Post("/gui", guiHandler)                                      // skipped
		router.Post("/gui/export", transactionHistoryCsvExport)              // skipped ?
		router.Post("/gui/alert/changeLock", alertChangeLockHandler)         // skipped
		router.Post("/gui/alert/initializeSeed", alertInitializeSeedHandler) // skipped
		router.Post("/gui/alert/sendCoins", alertSendCoinsHandler)           // skipped
		router.Post("/gui/alert/receiveCoins", alertReceiveCoinsHandler)     // migrated
		router.Post("/gui/alert/recoverSeed", alertRecoverSeedHandler)
		router.Post("/gui/alert/restoreFromSeed", alertRestoreFromSeedHandler) // skipped
		router.Post("/gui/changeLock", changeLockHandler)
		router.Post("/gui/collapseMenu", collapseMenuHandler)                       // skipped
		router.Post("/gui/deleteConsensus", deleteConsensusHandler)                 // migrated
		router.Post("/gui/deleteConsensusForm", deleteConsensusFormHandler)         // skipped
		router.Post("/gui/expandMenu", expandMenuHandler)                           // skipped
		router.Post("/gui/explainWhale", explainWhaleHandler)                       // skipped
		router.Post("/gui/importExportNotesForm", importExportNotesFormHandler)     // skipped
		router.Post("/gui/importExportNotesCancel", importExportNotesCancelHandler) // skipped
		router.Post("/gui/initializeSeed", initializeSeedHandler)                   // migrated
		router.Post("/gui/lockWallet", lockWalletHandler)
		router.Post("/gui/privacy", privacyHandler)
		router.Post("/gui/restoreSeed", restoreSeedHandler) // migrated
		router.Post("/gui/scanning", scanningHandler)       // skipped (use /api/v2/blockHeight)
		router.Post("/gui/sendCoins", sendCoinsHandler)
		router.Post("/gui/uploadMultispendCsvForm", uploadMultispendCsvFormHandler)
		router.Post("/gui/uploadMultispendCsv", uploadMultispendCsvHandler)
		router.Post("/gui/setTxHistoryPage", setTxHistoyPage)         // skipped
		router.Post("/gui/unlockWallet", unlockWalletHandler)         // migrated
		router.Post("/gui/unlockWalletForm", unlockWalletFormHandler) // skipped
		router.Post("/gui/explorer", explorerHandler)                 // migrated
		router.Post("/gui/balance", balanceHandler)                   // migrated
		router.Post("/gui/blockHeight", blockHeightHandler)           // migrated
		router.Post("/api/txHistoryPage", transactionHistoryJson)     // migrated

	}

	// New REST endpoints (JSON Controlled) - restApi.go
	// Moved outside the if statement to allow access to methods when consensus at all times
	router.Post("/rest/v1/unlock-wallet", allowCORS(restUnlockWallet))
	router.Get("/rest/v1/lock-wallet", allowCORS(restLockWallet))
	router.Get("/rest/v1/sessions", allowCORS(restSessions))
	router.Get("/rest/v1/wallets", allowCORS(restWallets))
	router.Get("/rest/v1/block-height", allowCORS(restBlockHeight))
	router.Get("/rest/v1/balance", allowCORS(restBalance))
	router.Post("/rest/v1/delete-consensus", allowCORS(restDeleteConsensus))
	router.Get("/rest/v1/transactions", allowCORS(restTransactions))
	router.Post("/rest/v1/transaction-details", allowCORS(restTransactionDetails))
	router.Get("/rest/v1/addresses", allowCORS(restAddresses))
	router.Get("/rest/v1/header-data", allowCORS(restHeaderData))
	router.Post("/rest/v1/initialize-seed", allowCORS(restInitializeSeed))
	router.Post("/rest/v1/restore-seed", allowCORS(restRestoreSeed))
	router.Post("/rest/v1/send-coins", allowCORS(restSendCoins))
	router.Post("/rest/v1/multi-send-coins-csv", allowCORS(restMultiSendCsv))
	router.Post("/rest/v1/change-password", allowCORS(restChangeWalletPassword))
	router.Get("/rest/v1/recover-seed", allowCORS(restRecoverSeed))
	router.Get("/rest/v1/consensus-status", allowCORS(restConsensusStatus))
	router.Post("/rest/v1/consensus-build", allowCORS(restConsensusBuild))
	router.Post("/rest/v1/export-csv", allowCORS(restTransactionHistoryCsvExport))
	// Transporter
	router.Post("/rest/v1/transporter/allowance", allowCORS(restTransporterAllowance))
	router.Post("/rest/v1/transporter/send", allowCORS(restTransporterSend))

	// Transaction Notes (CRUD)
	router.Post("/rest/v1/notes", allowCORS(restNotesJSON))   // Create
	router.Get("/rest/v1/notes", allowCORS(restNotesJSON))    // Read
	router.Put("/rest/v1/notes", allowCORS(restNotesJSON))    // Update
	router.Delete("/rest/v1/notes", allowCORS(restNotesJSON)) // Delete

	router.Get("/rest/v1/votes", allowCORS(restGetVoteOptions))
	router.Post("/rest/v1/votes", allowCORS(restCastVote))

	stripped, err := fs.Sub(frontend.Embed, "dist")
	if err != nil {
		fmt.Println(err)
	}
	fs := http.FileServer(http.FS(stripped))
	router.Handle("/index.html", fs)
	router.Handle("/favicon.ico", fs)
	router.Handle("/assets/*", fs)

	router.NotFound(func(w http.ResponseWriter, r *http.Request) {
		f, err := http.FS(stripped).Open("index.html")
		if err != nil {
			fmt.Printf("problem opening mounted file: %v", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		defer f.Close()

		var modtime time.Time
		if stat, err := f.Stat(); err == nil {
			modtime = stat.ModTime()
		}

		http.ServeContent(w, r, "", modtime, f)
		fmt.Println("serve end")
	})

	return router
}
