package frontend

import "embed"

// Embed returns a frontend embed.FS
//
//go:embed dist
var Embed embed.FS
