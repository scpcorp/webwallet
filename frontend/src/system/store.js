import {createStore} from "vuex";

function loadState() {
    try {
        const serializedState = localStorage.getItem('storeState');
        if (serializedState === null) {
            return undefined; // No state in localStorage, return initial state
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined; // Errors could happen while parsing
    }
}

function saveState(state) {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('storeState', serializedState);
    } catch (err) {
        // Handle errors here, like showing a notification to the user
    }
}

const store = createStore({
    state() {
        return loadState() || {
            session: {
                id: null,
                name: null,
                restore: false,
            },
            selectedWallet: null,
            selectedWalletAddress: null,
        }
    },
    getters: {
        getSessionId(state) {
            return state.session.id;
        },
        getSessionName(state) {
            return state.session.name;
        },
        isRestoring(state) {
            return state.session.restore || false;
        },
        hasSessionId(state) {
            return state.session.id !== null;
        },
        hasWalletLinked(state) {
            return state.selectedWallet !== null;
        },
        linkedWallet(state) {
            return state.selectedWallet;
        },
        solanaAddress(state) {
            return state.selectedWalletAddress;
        },
    },
    mutations: {
        setSession(state, value) {
            state.session.id = value.id;
            state.session.name = value.name;
            state.session.restore = value.restore || false;
        },
        setRestore(state, restore) {
            state.session.restore = restore;
        },
        setSelectedWallet(state, wallet) {
            if (wallet === null) {
                state.selectedWallet = null;
                state.selectedWalletAddress = null;
            } else {
                state.selectedWallet = wallet.name || null;
                state.selectedWalletAddress = wallet.address || null;
            }
        }
    },
    actions: {
        setSession({commit, state}, value) {
            commit('setSession', value);
        },
        setRestore({commit, state}, value) {
            commit('setRestore', value);
        },
        setSelectedWallet({commit, state}, value) {
            commit('setSelectedWallet', value);
        },
    },
});

store.subscribe((mutation, state) => {
    saveState(state);
});

export default store;
