import {createRouter, createWebHistory} from 'vue-router';

import Home from '../views/logged-out/Home.vue';
import UnlockWallet from "../views/logged-out/UnlockWallet.vue";
import RestoreFromSeed from "../views/logged-out/RestoreFromSeed.vue";
import CreateNewWallet from "../views/logged-out/CreateNewWallet.vue";
import RebuildConsensus from "../views/logged-out/RebuildConsensus.vue";

import ChangePassword from '../views/logged-in/ChangePassword.vue';
import RecoverSeed from '../views/logged-in/RecoverSeed.vue';
import Transactions from '../views/logged-in/Transactions.vue';
import SendCoins from '../views/logged-in/SendCoins.vue';
import MultiSendCoins from '../views/logged-in/MultiSendCoins.vue';
import ReceiveCoins from '../views/logged-in/ReceiveCoins.vue';
import SendTransporter from "../views/logged-in/SendTransporter.vue";
import Vote from "../views/logged-in/Vote.vue";
import store from "./store.js";

const routes = [
    {
        path: '/',
        children: [
            { path: '/', name: 'Home', component: Home },
            { path: 'unlock-wallet', name: 'UnlockWallet', component: UnlockWallet },
            { path: 'restore-from-seed', name: 'RestoreFromSeed', component: RestoreFromSeed },
            { path: 'create-new-wallet', name: 'CreateNewWallet', component: CreateNewWallet },
            // { path: 'create-cold-wallet', name: 'CreateColdWallet', component: UnlockWallet },
            { path: 'rebuild-consensus', name: 'RebuildConsensus', component: RebuildConsensus },
        ],
        meta: {
            requiresSessionId: false,
        }
    },
    {
        path: '/wallet/',
        redirect: 'transactions',
        children: [
            { path: 'change-password', name: 'ChangePassword', component: ChangePassword },
            { path: 'recover-seed', name: 'RecoverSeed', component: RecoverSeed },
            { path: 'transactions', name: 'Transactions', component: Transactions },
            { path: 'send-coins', name: 'SendCoins', component: SendCoins },
            { path: 'multi-send-coins', name: 'MultiSendCoins', component: MultiSendCoins },
            { path: 'transporter', name: 'SendTransporter', component: SendTransporter },
            { path: 'receive-coins', name: 'ReceiveCoins', component: ReceiveCoins },
            { path: 'vote', name: 'Vote', component: Vote },
        ],
        meta: {
            requiresSessionId: true,
        }
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
});

router.beforeEach(async function (to, from, next) {
    if (to.path === '/gui') {
        next({page: 'Home'});
    } else {
        if (to.meta.requiresSessionId) {
            if (store.getters.hasSessionId) {
                next();
            } else {
                next({page: 'Home'});
            }
        } else {
            next();
        }
    }
});

export default router;
