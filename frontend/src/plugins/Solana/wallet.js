import {BaseMessageSignerWalletAdapter} from "@solana/wallet-adapter-base";

export default class Wallet {
    wallet;
    walletAddress;
    name = '';
    image = '';

    /**
     * @param wallet {BaseMessageSignerWalletAdapter}
     */
    constructor(wallet) {
        this.wallet = wallet;
        this.name = wallet.name;
        this.image = wallet.icon;
    }

    async connect() {
        try {
            await this.wallet.connect();
            this.walletAddress = this.wallet.publicKey.toString();
            return true;
        } catch (error) {
            // console.error("Error connecting to wallet:", error);
            return false;
        }
    }

    async disconnect() {
        await this.wallet.disconnect();
    }

    get Address() {
        return this.walletAddress.substring(0, 5) + "..." + this.walletAddress.substring(this.walletAddress.length - 5);
    }

}
