import { SolflareWalletAdapter } from '@solana/wallet-adapter-wallets';
import { WalletAdapterNetwork } from '@solana/wallet-adapter-base';
import { Connection } from '@solana/web3.js';

const network = WalletAdapterNetwork.Mainnet; // Or Devnet/Testnet
const endpoint = 'https://api.mainnet-beta.solana.com/';
const connection = new Connection(endpoint);

const solflareAdapter = new SolflareWalletAdapter({ network });

export const connectWallet = async () => {
    try {
        await solflareAdapter.connect();
        console.log("Wallet connected", solflareAdapter.publicKey.toString());
    } catch (error) {
        console.error("Error connecting to wallet:", error);
    }
};

export const disconnectWallet = async () => {
    await solflareAdapter.disconnect();
    console.log("Wallet disconnected");
};

export const getWalletConnection = () => {
    return {
        publicKey: solflareAdapter.publicKey,
        connected: solflareAdapter.connected,
        connect: connectWallet,
        disconnect: disconnectWallet,
    };
};
