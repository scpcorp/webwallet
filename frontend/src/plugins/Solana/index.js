import { SolflareWalletAdapter } from '@solana/wallet-adapter-wallets';
import { WalletAdapterNetwork } from '@solana/wallet-adapter-base';
import {Connection} from "@solana/web3.js";
import Wallet from "./wallet.js";

const DefaultOptions = {
    wallets: [],
    autoConnect: false,
    initialCluster: WalletAdapterNetwork.Mainnet,
    endpoint: 'https://api.mainnet-beta.solana.com/'
}

const Solana = {
    async install(Vue, options = {}) {
        Vue.config.globalProperties.$solana = new SolanaWallet(options);
    },
}

class SolanaWallet {
    #connection;
    #walletListing = [];
    options = {};

    constructor(options) {
        this.options = Object.assign(DefaultOptions, options);
        this.#connection = new Connection(this.options.endpoint);
        for (let wallet of this.options.wallets) {
            this.#walletListing.push(new Wallet(wallet));
        }
    }

    listWallets() {
        return this.#walletListing;
    }

}

export default Solana;
