export default {
    created() {
    },
    computed: {
        hasSession() {
            return this.$store.getters['hasSessionId'];
        },
        getSessionId() {
            return this.$store.getters['getSessionId'];
        },
        getSessionName() {
            return this.$store.getters['getSessionName'];
        },
        isRestoring() {
            return this.$store.getters['isRestoring'];
        },
        hasWalletLinked() {
            return this.$store.getters['hasWalletLinked'];
        },
        linkedWallet() {
            return this.$store.getters['linkedWallet'];
        },
        solanaAddress() {
            return this.$store.getters['solanaAddress'];
        },
        getCoinTypes() {
            return [
                {key: 'SCP', value: 'SCP'},
                {key: 'SPFA', value: 'SPF-A'},
                {key: 'SPFB', value: 'SPF-B'},
            ]
        },
    },
    methods: {
        goto(page) {
            if (page.startsWith('/')) {
                this.$router.push(page);
            } else {
                this.$router.push({name: page});
            }
        },
        logout() {
            this.$store.dispatch('setSession', {
                id: null,
                name: null,
            });
            this.goto('Home');
        },
        formatCurrencyName(name) {
            switch (name.toLowerCase()) {
                case 'scp':
                    return 'SCP';
                case 'spfa':
                    return 'SPF-A';
                case 'spfb':
                    return 'SPF-B';
            }
        },
        triggerDownloadEvent(content, type, filename) {
            const blob = new Blob([content], { type: type });
            const link = document.createElement('a');
            const url = URL.createObjectURL(blob);
            link.setAttribute('href', url);
            link.setAttribute('download', filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}
