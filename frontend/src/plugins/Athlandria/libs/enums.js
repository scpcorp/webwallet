export class Enum {

    static get TABLE_FORMAT() {
        return {
            String: 'string',
            FileSize: 'filesize',
            Date: 'timestamp',
            DateOnly: 'datestamp',
            Integer: 'int',
            Float: 'float',
            Currency: 'currency',
            Input: 'input',
        }
    }

}
