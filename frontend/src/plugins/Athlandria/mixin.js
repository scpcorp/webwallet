import {getCurrentInstance, toRaw} from 'vue';
import EventBus from "./libs/EventBus.js";

export default {

    created() {
        // let localName = `${getCurrentInstance().type.name}${getCurrentInstance().type.__hmrId || ''}`
        // console.log(localName);
        if (this.isSocketComponent) {
            this.loadSocketCommunications();
        }
    },

    unmounted() {
        if (this.isSocketComponent) {
            this.$socket.unregisterAll(this.getLocalName);
        }
    },

    updated() {
        if (this.isSocketComponent) {
            this.loadSocketCommunications();
        }
    },

    computed: {
        getLocalName() {
            return `${getCurrentInstance().type.name}${getCurrentInstance().type.__hmrId || ''}`;
        },
        isSocketComponent() {
            return getCurrentInstance().type.socket !== undefined;
        },
        getSocketListens() {
            return getCurrentInstance().type.socket || [];
        },
        BUS() {
            return EventBus;
        },
        Modals() {
            return {
                attach: () => {
                    this.BUS.on('modal-closed', () => {
                        for (let modal in this.modals) {
                            this.modals[modal].show = false;
                            this.modals[modal].data = {};
                        }
                    });
                },
                close: () => {
                    for (let modal in this.modals) {
                        this.modals[modal].show = false;
                        this.modals[modal].data = {};
                    }
                }
            }
        }
    },

    methods: {
        loadSocketCommunications() {
            try {
                for (let event of this.getSocketListens) {
                    this.$socket.register(event, this.sendAction.bind(this), this.getLocalName);
                }
            } catch (e) {
                console.error(`[${getCurrentInstance().type.name}]`, 'This component is setup to receive socket events; however, the method: `sendAction(source, action, args)` is missing.');
                console.error(e);
            }
            console.debug(this.getLocalName, this.getSocketListens);
        },
        showDialog(question, type, onYes = null, onNo = null, timeout = 0) {
            this.BUS.emit('uiDialog', {
                question: question,
                type: type,
                yes: onYes,
                no: onNo,
                timeout: timeout,
            });
        },
        showPrompt(question, type, defaultValue, onOK) {
            this.BUS.emit('uiPrompt', {
                question: question,
                type: type,
                default: defaultValue,
                ok: onOK,
            });
        },
        showToast(message, success = true, title = null, delay = 5000) {
            this.BUS.emit('sendToast', {
                title: title,
                message: message,
                success: success,
                delay: delay,
            });
        },
        copyToClipboard(data) {
            if (navigator.clipboard) {
                navigator.clipboard.writeText(data);
                this.showToast('Text copied to your clipboard.');
            } else {
                let toBeCopied = document.createElement("textarea");
                toBeCopied.value = data;
                document.body.appendChild(toBeCopied);
                toBeCopied.style.top = "0";
                toBeCopied.style.left = "0";
                toBeCopied.style.position = "fixed";
                toBeCopied.focus();
                toBeCopied.select();
                document.execCommand("copy");
                toBeCopied.style.display = "none";
                this.showToast('Text copied to your clipboard.');
            }
        },
        convertToCSV(data) {
            const csvRows = [];
            const headers = Object.keys(data[0]);
            csvRows.push(headers.join(',')); // Add headers row

            for (const row of data) {
                const values = headers.map(header => {
                    const escaped = (''+row[header]).replace(/"/g, '\\"'); // Escape double quotes
                    return `"${escaped}"`;
                });
                csvRows.push(values.join(','));
            }

            return csvRows.join('\n');
        },
        pad(num, size) {
            num = num.toString();
            while (num.length < size) num = "0" + num;
            return num;
        }
    }

}
