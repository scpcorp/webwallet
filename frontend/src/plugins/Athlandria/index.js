import { defineAsyncComponent } from 'vue';
import BaseMixin from './mixin.js';
import CommunicationsArray from "./tools/CommunicationsRest.js";

const DefaultOptions = {
    devPort: 3000,
    keycloak: null,
    bootstrap: true,
    type: 'socket',
}

const Athlandria = {

    Directive: {
        SOCKET: 'socket',
        REST: 'rest',
    },

    async install(Vue, options = {}) {
        options = Object.assign(DefaultOptions, options);
        if (options.type === undefined) {
            options.type = Athlandria.Directive.SOCKET;
        }

        // console.log("Athlandria Options", options);
        // const CommunicationsArray = await this.loadCommunications(options);
        Vue.use(CommunicationsArray, options);
        if (options.bootstrap) {
            // console.info("Bootstrap Required.");

            Vue.component('BootstrapCard', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapCard.vue')
            ));
            Vue.component('BootstrapModal', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapModal.vue')
            ));
            Vue.component('BootstrapInput', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapInput.vue')
            ));
            Vue.component('BootstrapTable', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapTable.vue')
            ));
            Vue.component('BootstrapInputRange', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapInputRange.vue')
            ));
            Vue.component('BootstrapSelect', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapSelect.vue')
            ));
            Vue.component('BootstrapTabs', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapTabs.vue')
            ));
            Vue.component('BootstrapTextArea', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapTextArea.vue')
            ));
            Vue.component('BootstrapToast', defineAsyncComponent(() =>
                import('./components/bootstrap/BootstrapToast.vue')
            ));
            Vue.component('AddyCheckbox', defineAsyncComponent(() =>
                import('./components/addy/AddyCheckbox.vue')
            ));

            // const BS = await this.loadBootstrapRequirements();
            // Vue.component('BootstrapCard', BS.BootstrapCard);
            // Vue.component('BootstrapModal', BS.BootstrapModal);
            // Vue.component('BootstrapInput', BS.BootstrapInput);
            // Vue.component('BootstrapTable', BS.BootstrapTable);
            // Vue.component('BootstrapInputRange', BS.BootstrapInputRange);
            // Vue.component('BootstrapSelect', BS.BootstrapSelect);
            // Vue.component('BootstrapTabs', BS.BootstrapTabs);
            // Vue.component('BootstrapTextArea', BS.BootstrapTextArea);
            // Vue.component('BootstrapToast', BS.BootstrapToast);
            // Vue.component('AddyCheckbox', BS.AddyCheckbox);
        }
        Vue.mixin(BaseMixin);
    },

    async loadBootstrapRequirements() {
        return {
            BootstrapCard: await import("./components/bootstrap/BootstrapCard.vue"),
            BootstrapModal: await import("./components/bootstrap/BootstrapModal.vue"),
            BootstrapInput: await import("./components/bootstrap/BootstrapInput.vue"),
            BootstrapTable: await import("./components/bootstrap/BootstrapTable.vue"),
            BootstrapInputRange: await import("./components/bootstrap/BootstrapInputRange.vue"),
            BootstrapTextArea: await import("./components/bootstrap/BootstrapTextArea.vue"),
            AddyCheckbox: await import("./components/addy/AddyCheckbox.vue"),
            BootstrapSelect: await import("./components/bootstrap/BootstrapSelect.vue"),
            BootstrapTabs: await import("./components/bootstrap/BootstrapTabs.vue"),
            BootstrapToast: await import("./components/bootstrap/BootstrapToast.vue"),
        }
    },

    async loadCommunications(options) {
        switch (options.type) {
            case Athlandria.Directive.SOCKET:
                return await import("./tools/CommunicationsSocket.js");
            case Athlandria.Directive.REST:
                return await import("./tools/CommunicationsRest.js");
        }
    },

}

export default Athlandria;
