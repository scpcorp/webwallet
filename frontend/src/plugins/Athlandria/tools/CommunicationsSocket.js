import { io } from "socket.io-client";
import EventBus from "../libs/EventBus";
// import jwtDecode from "jwt-decode";

class CommunicationsSocket {
    #options = {};
    socket;
    eventListing = {};

    constructor(options) {
        this.#options = options;
        console.log("CommunicationsSocket Loaded", this.#options);
        if (this.#options.keycloak === null) {
            this.#connect();
        } else {
            if (this.#testKeycloak()) {
                this.#connect();
            } else {
                this.#retryKeycloak();
            }
        }

        EventBus.on('TokenRefresh', () => {
            try {
                this.#disconnect();
                this.#connect(true);
            } catch (e) {
                this.#connect(true);
            }
        });
    }

    #testKeycloak() {
        let kcToken = localStorage.getItem('keycloakToken');
        if (kcToken !== null) {
            let token = jwtDecode(kcToken);
            let exp = new Date(token.exp * 1000);
            if (exp < new Date()) {
                // Token expired, remove it...
                localStorage.removeItem('keycloakToken');
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    #disconnect() {
        this.socket.disconnect();
    }

    #connect(isReconnect = false) {
        let kcToken = this.#options.keycloak ? localStorage.getItem('keycloakToken') : null;
        if (this.isDevelopment()) {
            console.log("[Comms]", "DEV MODE ENABLED");
            this.socket = io(`ws://${window.location.hostname}:${this.#options.devPort}/`, {
                extraHeaders: {
                    'keycloak-token': kcToken,
                }
            });
        } else {
            this.socket = io({
                extraHeaders: {
                    'keycloak-token': kcToken,
                }
            });
        }
        this.socket.on("connect_error", () => {
            // revert to classic upgrade
            this.socket.io.opts.transports = ["polling", "websocket"];
        });
        this.socket.on('handshakeResponse', (resp) => {
            if (this.isDevelopment()) {
                if (resp.success) {
                    console.debug('[Comms] Successful handshake');
                } else {
                    console.debug('[Comms] Failed handshake');
                }
            }
            if (isReconnect === false) {
                if (resp.success) {
                    EventBus.emit('GlobalSystemStats', 'Ready', resp.systemVersion);
                } else {
                    EventBus.emit('GlobalSystemStats', 'Error', 'Handshake failure');
                }
            }
        });
        this.socket.on('message', (message) => {
            // console.log('message', message, this.eventListing[message.target]);
            for (let pos in this.eventListing[message.target]) {
                let row = this.eventListing[message.target][pos];
                row(message.target, message.requester || message.action, message.args);
            }
        });
        this.socket.connect();
        this.socket.emit('handshake', {});
    }

    #retryKeycloak() {
        setTimeout(() => {
            console.log("Retrying connection...");
            if (this.#testKeycloak()) {
                this.#connect();
            } else {
                this.#retryKeycloak();
            }
        }, 500);
    }

    send(target, action, args = {}, requester = null) {
        this.socket.emit('message', {target: target, action: action, args: args, requester: requester});
    }

    register(event, action, parentClass) {
        if (this.eventListing[event] === undefined) {
            this.eventListing[event] = {};
        }
        this.eventListing[event][parentClass] = action;
    }

    unregister(event, parentClass) {
        delete this.eventListing[event][parentClass];
    }

    unregisterAll(parentClass) {
        for (let event in this.eventListing) {
            delete this.eventListing[event][parentClass];
        }
    }

    isDevelopment() {
        return process.env.NODE_ENV === 'development';
    }

}

export default {
    install: (app, options) => {
        app.config.globalProperties.$socket = new CommunicationsSocket(options);
    }
}
