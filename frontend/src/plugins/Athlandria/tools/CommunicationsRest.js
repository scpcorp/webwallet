import axios from 'axios';

class CommunicationsRest {
    #options = {};

    constructor(options) {
        this.#options = options;
        // console.log("CommunicationsRest Loaded", this.#options);
    }

    async request(endpoint, method = 'GET', data = null, timeout = 20000) {
        try {
            let options = {
                method: method,
                url: `${this.base}${endpoint}`,
                headers: this.headers,
                timeout: timeout || 20000,
                validateStatus: false,
                withCredentials: false,
            };

            if (data !== null) {
                options.data = data;
            }
            // console.log('Options', options);
            let request = await axios(options);
            return request.data;
        } catch (e) {
            console.error("Error Detected", e.message);
            return {success: false, message: e.message};
        }
    }

    get base() {
        if (this.#options.rest.host.endsWith('/')) {
            return this.#options.rest.host.slice(0, -1);
        } else {
            return this.#options.rest.host;
        }
    }

    get headers() {
        return this.#options.rest.headers || null;
    }

}

export default {
    install: (app, options) => {
        app.config.globalProperties.$rest = new CommunicationsRest(options);
    }
}
