// src/wallets.js
import { SolflareWalletAdapter } from '@solana/wallet-adapter-wallets';
import { computed } from 'vue';

export const wallets = computed(() => [
    new SolflareWalletAdapter({ network: 'mainnet-beta' }), // You can specify the network as needed
]);
