import {createApp} from 'vue';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';
// import "bootstrap/js/src/dropdown.js";

import './theme/scp-theme/core.scss';

import App from './App.vue';

import Athlandria from "./plugins/Athlandria/index.js";
import Solana from "./plugins/Solana/index.js";
import router from "./system/router.js";
import store from './system/store.js';
import mixin from "./plugins/mixin.js";

import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';
import {fab} from '@fortawesome/free-brands-svg-icons';
import {
    CoinbaseWalletAdapter,
    FractalWalletAdapter, ParticleAdapter,
    PhantomWalletAdapter,
    SkyWalletAdapter,
    SolflareWalletAdapter
} from "@solana/wallet-adapter-wallets";
import {WalletAdapterNetwork} from "@solana/wallet-adapter-base";

library.add(fas, far, fab);

const baseLocation = `${window.location.protocol}//${window.location.host}/`;

const app = createApp(App);
app.use(Athlandria, {
    bootstrap: true,
    type: Athlandria.Directive.REST,
    rest: {
        host: (process.env.NODE_ENV === 'development' ? 'http://10.0.60.222:4300/' : baseLocation),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
    }
});

app.use(Solana, {
    wallets: [
        new PhantomWalletAdapter({ network: WalletAdapterNetwork.Mainnet }),
        new SolflareWalletAdapter({ network: WalletAdapterNetwork.Mainnet }),
        new FractalWalletAdapter({ network: WalletAdapterNetwork.Mainnet }),
        new SkyWalletAdapter({ network: WalletAdapterNetwork.Mainnet }),
        new ParticleAdapter({ network: WalletAdapterNetwork.Mainnet }),
        new CoinbaseWalletAdapter({ network: WalletAdapterNetwork.Mainnet }),
    ],
    autoConnect: true,
})

app.component('font-awesome-icon', FontAwesomeIcon);
app.use(store);
app.use(router);
app.mixin(mixin);

app.mount('#app');

