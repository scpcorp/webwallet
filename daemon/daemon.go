package daemon

import (
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/driver/desktop" //nolint:all
	"fyne.io/fyne/v2/widget"
	"github.com/ncruces/zenity"
	spdBuild "gitlab.com/scpcorp/ScPrime/build"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/node"
	"gitlab.com/scpcorp/webwallet/build"
	"gitlab.com/scpcorp/webwallet/resources"
	"gitlab.com/scpcorp/webwallet/server"
	wwConfig "gitlab.com/scpcorp/webwallet/utils/config"
)

// printVersionAndRevision prints the daemon's version and revision numbers.
func printVersionAndRevision() {
	if build.Version == "" {
		fmt.Println("WARN: compiled ScPrime web wallet without version.")
	} else {
		fmt.Println("ScPrime web wallet v" + build.Version)
	}
	if build.GitRevision == "" {
		fmt.Println("WARN: compiled ScPrime web wallet without version.")
	} else {
		fmt.Println("ScPrime web wallet Git revision " + build.GitRevision)
	}
	if spdBuild.DEBUG {
		fmt.Println("Running ScPrime daemon with debugging enabled")
	}
	if spdBuild.Version == "" {
		fmt.Println("WARN: compiled ScPrime daemon without version.")
	} else {
		fmt.Println("ScPrime daemon v" + spdBuild.Version)
	}
}

// installMmapSignalHandler installs a signal handler for Mmap related signals
// and exits when such a signal is received.
func installMmapSignalHandler() {
	// NOTE: ideally we would catch SIGSEGV here too, since that signal can
	// also be thrown by an mmap I/O error. However, SIGSEGV can occur under
	// other circumstances as well, and in those cases, we will want a full
	// stack trace.
	mmapChan := make(chan os.Signal, 1)
	signal.Notify(mmapChan, syscall.SIGBUS)
	go func() {
		<-mmapChan
		fmt.Println("A fatal I/O exception (SIGBUS) has occurred.")
		fmt.Println("Please check your disk for errors.")
		os.Exit(1)
	}()
}

// installKillSignalHandler installs a signal handler for os.Interrupt, syscall.SIGTERM
// and returns a channel that is closed when one of them is caught.
func installKillSignalHandler() chan os.Signal {
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	return sigChan
}

func startNode(node *node.Node, config *wwConfig.WebWalletConfig, loadStart time.Time) {
	err := loadNode(node, config)
	if err != nil {
		fmt.Println("Server is unable to create the ScPrime node.")
		fmt.Println(err)
		return
	}
	// Print a 'startup complete' message.
	startupTime := time.Since(loadStart)
	fmt.Printf("Finished full startup in %.3f seconds\n", startupTime.Seconds())
}

func isPortAvailabile(config *wwConfig.WebWalletConfig) (bool, error) {
	port := config.Port
	url := "http://" + net.JoinHostPort("localhost", strconv.Itoa(port))
	client := http.Client{Timeout: time.Duration(50) * time.Millisecond}
	optReq, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		return false, err
	}
	res, err := client.Do(optReq)
	if err == nil {
		res.Body.Close()
		for _, app := range res.Header.Values("Application") {
			return false, fmt.Errorf("port %d already in use by %s", port, app)
		}
		return false, fmt.Errorf("port %d already in use", port)
	}
	res, err = http.Get(url)
	if err == nil {
		res.Body.Close()
		for _, app := range res.Header.Values("Application") {
			return false, fmt.Errorf("port %d already in use by %s", port, app)
		}
		return false, fmt.Errorf("port %d already in use", port)
	}
	return true, nil
}

// StartDaemon uses the config parameters to initialize modules and start the web wallet.
func StartDaemon(config *wwConfig.WebWalletConfig) error {

	// record startup time
	loadStart := time.Now()

	// print the Version and GitRevision
	printVersionAndRevision()
	fmt.Printf("spd build release: %s\n", spdBuild.Release)
	fmt.Printf("webwallet build release: %s\n", build.Release)

	// install a signal handler that will catch exceptions thrown by mmap'd files
	installMmapSignalHandler()

	// verify port is open
	resp, err := isPortAvailabile(config)
	if err != nil || !resp {
		fmt.Printf("Port %d is not available, quitting...\n", config.Port)
		return err
	}

	// start server
	fmt.Println("Starting ScPrime Web Wallet server...")
	server.StartHTTPServer(config)

	// start a node
	node, errChan := node.New(node.NodeParams{}, loadStart)
	if err := modules.PeekErr(errChan); err != nil {
		return err
	}
	if server.IsRunning() {
		go startNode(node, config, loadStart)
	}

	if server.IsRunning() {
		// block until node is started or 500 milliseconds has passed.
		for i := 0; i < 100; i++ {
			if node.TransactionPool == nil {
				time.Sleep(5 * time.Millisecond)
			}
		}
	}

	if !server.IsRunning() {
		fmt.Println("Unable to start server, quitting...")
		return nil
	}

	listenString := fmt.Sprintf("http://localhost:%d", config.Port)
	if config.Headless {
		fmt.Printf("SCP Web Wallet is running at %s\n", listenString)
	}

	// launch GUI
	if !config.Headless {
		var scpURL, err = url.Parse(fmt.Sprintf("%s/wallet/transactions", listenString))
		if err != nil {
			fmt.Println("Unable to parse listen string, quitting...")
			return nil
		}
		a := app.NewWithID("me.scpri.webwallet")
		w := a.NewWindow("SysTray")
		a.SetIcon(resources.SCPIcon())
		if desk, ok := a.(desktop.App); ok {
			m := fyne.NewMenu("ScPrime Webwallet",
				fyne.NewMenuItem("Show in browser", func() {
					a.OpenURL(scpURL)
				}))
			desk.SetSystemTrayMenu(m)
		}

		w.SetContent(widget.NewLabel("Fyne System Tray"))
		w.SetCloseIntercept(func() {
			w.Hide()
		})

		a.OpenURL(scpURL)
		a.Run()
	} else {
		sigChan := installKillSignalHandler()
		<-sigChan
		fmt.Println("\rCaught stop signal, quitting...")
	}

	// Close
	var shutdownGui zenity.ProgressDialog
	if !config.Headless {
		title := "Shutting Down ScPrime Web Wallet"
		shutdownGui, _ = zenity.Progress(zenity.Title(title), zenity.Pulsate())
		shutdownGui.Text("Closing node...")

	}

	server.CloseAllWallets()
	if node != nil {
		closeNode(node, config)
	}
	if shutdownGui != nil {
		shutdownGui.Complete()
		shutdownGui.Close()
	}

	return nil
}
